package com.practice.phoneBook;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Valeriy on 30.10.2016.
 */
public class PhoneBook implements Serializable{
    private List<Record> records = new LinkedList<>();
    private static final long serialVersionUID = 43L;

    @Override
    public String toString() {
        return "PhoneBook" +
                records.toString();
    }

    //operations
    public void addRecord(Record record){
        record.setNumber(Integer.toUnsignedLong(records.size()+1));
        records.add(record);
    }

    public void removeRecord(Record record){
        this.records.remove(record);
        Iterator<Record> it =records.iterator();
        int i=1;
        while (it.hasNext()) it.next().setNumber(Integer.toUnsignedLong(i++));
    }

    public boolean contains(Record record){return records.contains(record);}

    //find
    public Record findByNumber(Long number){
        for (Record rec : records) {
            if (rec.getNumber().equals(number)) return rec;
        }
        return null;
    }

    public List<Record> findByFullName(String name){
        List<Record> temp = new LinkedList<>();
        for(Record rec:records){
            if(rec.getFullName().contains(name)) temp.add(rec);
        }
        return temp;
    }

    public List<Record> findByPhoneNumber(String phoneNumber){
        List<Record> temp = new LinkedList<>();
        for(Record rec:records){
            if(rec.getPhoneNumber().equals(phoneNumber)) temp.add(rec);
        }
        return temp;
    }

    public List<Record> findByAddress(String address){
        List<Record> temp = new LinkedList<>();
        for(Record rec:records){
            if(rec.getAddress().contains(address)) temp.add(rec);
        }
        return temp;
    }
}
