package com.practice.phoneBook;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Valeriy on 30.10.2016.
 */
public class Main {
    static PhoneBook phoneBook = new PhoneBook();

    //io
    public static void writeToFile() throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("phoneBook.txt"));
        oos.writeObject(phoneBook);
        oos.close();
    }

    public static PhoneBook readFromFile() throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("phoneBook.txt"));
        PhoneBook temp=(PhoneBook)ois.readObject();
        ois.close();
        return temp;
    }

    //operations
    public static void createRecord(BufferedReader reader) throws IOException {
        Record temp = new Record();
        System.out.println("Enter first name:");
        temp.setFirstName(reader.readLine());
        System.out.println("Enter second name:");
        temp.setSecondName(reader.readLine());
        System.out.println("Enter surname:");
        temp.setSurname(reader.readLine());
        System.out.println("Enter phone pumber");
        temp.setPhoneNumber(reader.readLine());
        System.out.println("Enter address");
        temp.setAddress(reader.readLine());
        temp.createFullName();
        if(phoneBook.contains(temp)) {
            System.out.println("--- Such record already exists! ---");
        }
        else
        {
            phoneBook.addRecord(temp);
            System.out.println("--- Done! ---");
        }
    }

    public static void updateRecord(BufferedReader reader) throws IOException {
        try {
            Record temp = phoneBook.findByNumber(Long.parseLong(reader.readLine()));
            if (temp == null) {
                System.out.println("--- No record with such number! ---");
            } else {
                String st;
                System.out.println("Enter new first name:");
                st = reader.readLine();
                if (!st.equals("")) temp.setFirstName(st);
                System.out.println("Enter new second name:");
                st = reader.readLine();
                if (!st.equals("")) temp.setSecondName(st);
                System.out.println("Enter new surname:");
                st = reader.readLine();
                if (!st.equals("")) temp.setSurname(st);
                System.out.println("Enter new phone number");
                st = reader.readLine();
                if (!st.equals("")) temp.setPhoneNumber(st);
                System.out.println("Enter new address");
                st = reader.readLine();
                if (!st.equals("")) temp.setAddress(st);
                temp.createFullName();
                phoneBook.removeRecord(phoneBook.findByNumber(temp.getNumber()));
                phoneBook.addRecord(temp);
                System.out.println("--- Done! ---");
            }
        }
        catch (NumberFormatException e){
            System.out.println("Number expected");
        }
    }

    public static void removeRecord(BufferedReader reader) throws IOException {
        try {
            String st = reader.readLine();
            Record temp = phoneBook.findByNumber(Long.parseLong(st));
            if (temp == null) {
                System.out.println("--- No record with such number! ---");
            } else {
                phoneBook.removeRecord(phoneBook.findByNumber(temp.getNumber()));
                System.out.println("--- Done! ---");
            }
        }
        catch (NumberFormatException e){
            System.out.println("Number expected");
        }
    }

    public static void findRecord(BufferedReader reader) throws IOException {
        String st="",choice = "";
        List<Record> result = new LinkedList<>();
        switch (reader.readLine()){
            case "1":
                choice="Find by full name";
                System.out.println("Enter full name");
                st=reader.readLine();
                result=phoneBook.findByFullName(st);
                if(result.size()==0)  System.out.println("--- No records with such full name! ---");
                else System.out.println(result);
                break;
            case "2":
                choice="Find by phone number";
                System.out.println("Enter phone number");
                st=reader.readLine();
                result=phoneBook.findByPhoneNumber(st);
                if(result.size()==0)  System.out.println("--- No records with such phone number! ---");
                else System.out.println(result);
                break;
            case "3":
                choice="Find by address";
                System.out.println("Enter address:");
                st=reader.readLine();
                result=phoneBook.findByAddress(st);
                if(result.size()==0)  System.out.println("--- No records with such address! ---");
                else System.out.println(result);
                break;
            default:
                System.out.println("Wrong input");
                break;
        }

        if(!choice.equals("")&&result.size()!=0) {
            System.out.println("Do you want save results to a file?");
            switch (reader.readLine()) {
                case "yes":
                    FileWriter fw = new FileWriter("searchResult.txt");
                    fw.write(choice+": "+st);
                    for (Record rec : result) {
                        fw.write(rec.toString());
                    }
                    fw.close();
                    System.out.println("--- Done! ---");
                    break;
                case "no":
                    break;
                default:
                    System.out.println("Wrong input");
            }
        }
    }

    //test
    public static void test() throws IOException, ClassNotFoundException {

        System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Create 3 records:\n");
        Record record = new Record();
        Record record2 = new Record();
        Record record3 = new Record();

        record.setAddress("Dnipro,Gagarina 47/38");
        record.setFirstName("Viktor");
        record.setSecondName("Rodionovich");
        record.setSurname("Ivanov");
        record.setPhoneNumber("+34845832");
        record.createFullName();

        record2.setAddress("Dnipro,Gagarina 95/3");
        record2.setFirstName("Ivan");
        record2.setSecondName("Petrovich");
        record2.setSurname("Kruglov");
        record2.setPhoneNumber("+34765832");
        record2.createFullName();

        record3.setAddress("Dnipro,Chkalova 56/21");
        record3.setFirstName("Roman");
        record3.setSecondName("Vitalievich");
        record3.setSurname("Kruglov");
        record3.setPhoneNumber("+34765832");
        record3.createFullName();

        phoneBook.addRecord(record);
        phoneBook.addRecord(record2);
        phoneBook.addRecord(record3);
        System.out.println(phoneBook);
        System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Delete " + record2 + ":\n");
        phoneBook.removeRecord(record2);
        System.out.println(phoneBook);
        System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Contains"+record3+":\n");
        System.out.println(phoneBook.contains(record3));
        System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("FindByNumber 2:");
        System.out.println(phoneBook.findByNumber(2L));
        System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("FindByFullName \"Krug\":");
        System.out.println(phoneBook.findByFullName("Krug"));
        System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("FindByPhoneNumber +34845832:\n");
        System.out.println(phoneBook.findByPhoneNumber("+34765832"));
        System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("FindByAddress Dnipro:\n");
        System.out.println(phoneBook.findByAddress("Dnipro"));
        System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Deseriazized phoneBook:");
        writeToFile();
        PhoneBook temp=readFromFile();
        System.out.println(temp);
        System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
    }

    //console
    public static void createConsoleMenu() throws IOException, ClassNotFoundException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line = "";
        System.out.println("--- Start of the program ---");
        phoneBook=readFromFile();
        while (!line.equals("exit")){
            System.out.println("Enter the command:");
            line = br.readLine();
            switch (line){
                case "exit":
                    System.out.println("Good bye");
                    writeToFile();
                    br.close();
                    break;
                case "print":
                    System.out.println(phoneBook);
                    break;
                case "add":
                    System.out.println("Creating new record: ");
                    createRecord(br);
                    break;
                case "update":
                    System.out.println("Update record with number:");
                    updateRecord(br);
                    break;
                case "remove":
                    System.out.println("Remove record with number:");
                    removeRecord(br);
                    break;
                case "find":
                    System.out.println("Find record by (1. Name, 2. PhoneNumber, 3. Address):");
                    findRecord(br);
                    break;
                default:
                    System.out.println("Wrong command");
            }
        }
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        //test();
        createConsoleMenu();
    }
}
