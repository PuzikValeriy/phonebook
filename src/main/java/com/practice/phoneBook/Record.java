package com.practice.phoneBook;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

/**
 * Created by Valeriy on 28.10.2016.
 */
public class Record implements Serializable{
    private static final long serialVersionUID = 42L;
    transient private String firstName;
    transient private String secondName;
    transient private String surname;
    private Long number=0L;
    private String fullName;
    private String phoneNumber;
    private String address;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getFullName() {
        return fullName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void  createFullName(){ fullName = surname +" "+firstName +" "+secondName; }

    private void setFields(){
        String[] names = this.fullName.split(" ");
        this.surname=names[0];
        this.firstName=names[1];
        this.secondName=names[2];
    }

    @Override
    public String toString() {
        return  "\n[firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", surname='" + surname + '\'' +
                ", number=" + number +
                ", fullName='" + fullName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address='" + address + '\'' +
                ']';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Record record = (Record) o;

        if (address != null ? !address.equals(record.address) : record.address != null) return false;
        if (fullName != null ? !fullName.equals(record.fullName) : record.fullName != null) return false;
        if (phoneNumber != null ? !phoneNumber.equals(record.phoneNumber) : record.phoneNumber != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = fullName != null ? fullName.hashCode() : 0;
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        return result;
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
                in.defaultReadObject();
                String[] names = this.fullName.split(" ");
                this.surname=names[0];
                this.firstName=names[1];
                this.secondName=names[2];
            }
}
